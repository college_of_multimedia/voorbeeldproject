const mix = require('laravel-mix');

mix.setPublicPath('dist')

  .js('assets/js/app.js', 'dist/js')

  .sass('assets/scss/app.scss', 'dist/css', {
    includePaths: [
      "node_modules/bootstrap/scss",
    ]
  })

  .copyDirectory('assets/img', 'dist/img')

  .sourceMaps(true)

  .webpackConfig({
    devtool: 'source-map',
  })

  .version()

  .options({
    processCssUrls: false
  })
;
