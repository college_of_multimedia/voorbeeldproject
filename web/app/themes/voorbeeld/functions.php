<?php

namespace Voorbeeld;

/**
 * Define the defaults
 */
define('D_VERSION', 0.1);
define('D_THEME', 'voorbeeld');
define('D_DOMAIN', 'vb');
define('D_NAME', 'Voorbeeld');
define('D_PATH', __DIR__.'/Voorbeeld/');

/**
 * AutoLoad
 */
spl_autoload_register(function ($class) {
    if (class_exists($class)) {
        return;
    }
    $filepath = str_replace('\\', '/', $class).'.php';

    if (file_exists(__DIR__.'/'.$filepath)) {
        require_once(__DIR__.'/'.$filepath);
    }
});

new Theme\Support();
new Theme\AdvancedCustomFields();
new Theme\Rewrite();
new WooCommerce\Price();
new WooCommerce\Templates();

new PostTypes\Brand();
new PostTypes\Reseller();
new PostTypes\Product();
new PostTypes\Reseller();
new PostTypes\ResellerUser();

new Front\Cleanup();
new Front\EnqueueScripts();
//new Front\Search();
new Front\Filters();
new Front\Template();
new Front\GravityForms();

new Helpers\Flush();
new Helpers\StringFunctions();
new Helpers\UserState();
new Admin\Purge();

new Ajax\Product();
//
if (is_admin()) {
    new WooCommerce\Admin();
    new Admin\EnqueueScripts();
    new Admin\Activator();
    new Admin\Cleanup();
    new Admin\Menu();
    new Admin\ResellerUser();
    new Admin\Products();
}

include_once(D_PATH.'Helpers/Debug.php');
