<footer>
    <div class="row px-3 px-md-4 px-lg-5">
        <div class="col-12">
            <p class="font-size-xsmall">
                &copy; <?= date('Y') ?> Voorbeeld
                <a href="<?= get_privacy_policy_url() ?>">privacy statement</a>
            </p>
        </div>
    </div>
</footer>

<div id="users-device-size">
    <div id="xs" class="d-xs-block d-sm-none"></div>
    <div id="sm" class="d-none d-sm-block d-md-none"></div>
    <div id="md" class="d-none d-md-block d-lg-none"></div>
    <div id="lg" class="d-none d-lg-block"></div>
</div>
<?php
wp_footer();
?>
</body>
</html>
