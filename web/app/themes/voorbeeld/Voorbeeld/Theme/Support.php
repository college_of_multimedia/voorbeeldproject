<?php

namespace Voorbeeld\Theme;

class Support
{
    /**
     * The actions for this settings
     */
    public function __construct()
    {
        add_theme_support('post-thumbnails');

        add_action('after_setup_theme', [$this, 'setLanguageLocation']);
        add_action('after_setup_theme', [$this, 'afterThemeSetup']);
        add_action('after_setup_theme', [$this, 'registerMenus']);

        add_action('widgets_init', [$this, 'widgetSetup']);
        add_action('init', [$this, 'addSupport']);

        add_action('wp_head', [$this, 'startSession'], 1);

        $this->setImageSizes();
    }

    /**
     * The image sizes for this theme
     */
    public function setImageSizes()
    {
        add_image_size('icon', 50, 50, false);
        add_image_size('header_image', 2200, 300, ['center', 'center']);
        add_image_size('brand_logo', 500, 200, false);
    }

    /**
     * Start the session
     */
    public function startSession()
    {
        if (! session_id()) {
            session_start();
        }
    }

    /**
     * add theme support
     */
    public function addSupport()
    {
        add_post_type_support('page', 'excerpt');
        add_post_type_support('post', 'excerpt');
        add_theme_support('post-thumbnails');
        add_theme_support('disable-custom-font-sizes');
//        add_theme_support( 'disable-custom-colors' );
    }

    /**
     * after theme setup
     */
    public function afterThemeSetup()
    {
        add_theme_support('title-tag');
    }

    /**
     * Set the language location
     */
    public function setLanguageLocation()
    {
        load_theme_textdomain(D_DOMAIN, get_template_directory().'/languages');
    }

    /**
     * Add custom logo
     */
    public function customLogo()
    {
        add_theme_support(
            'custom-logo',
            [
                'height'      => 36,
                'width'       => 255,
                'flex-height' => true,
                'flex-width'  => true,
                'header-text' => ['site-title', 'site-description'],
            ]
        );
    }

    /**
     * Register the menu's
     *
     * Top menu
     */
    public function registerMenus()
    {
        register_nav_menus(
            [
                'primary'      => __('Main Menu', D_DOMAIN),
                'footer-col-1' => __('Footer column 1', D_DOMAIN),
                'footer-col-2' => __('Footer column 2', D_DOMAIN),
                'footer-col-3' => __('Footer column 3', D_DOMAIN),
                'footer-col-4' => __('Footer column 4', D_DOMAIN),
            ]
        );
    }

    /**
     * register sidebar
     */
    public function widgetSetup()
    {
        register_sidebar(
            [
                'name'          => __('Footer', D_DOMAIN),
                'id'            => 'footer',
                'description'   => __('Widgets that will be shown in the footer on all the pages.', D_DOMAIN),
                'before_widget' => '<div id="%1$s" class="col mb-2 widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h5 class="widget__title mb-3 text-uppercase">',
                'after_title'   => '</h5>',
            ]
        );

        register_sidebar(
            [
                'name'          => __('Sidebar', D_DOMAIN),
                'id'            => 'sidebar-1',
                'description'   => __('Default widget sidebar.', D_DOMAIN),
                'class'         => '',
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'  => '<h3 class="widget__title">',
                'after_title'   => '</h3>',
            ]
        );
    }
}
