<?php

namespace Voorbeeld\Front;

class EnqueueScripts
{

    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_action('wp_print_styles', [$this, 'setBackground'], 11);
    }

    /**
     * Load the correct javascipts and CSS
     */
    public function enqueueScripts()
    {
        wp_enqueue_style(
            'fonts',
            'https://use.typekit.net/tua5nrc.css',
            [],
            apply_filters('release_version', D_VERSION)
        );
        wp_enqueue_style(
            D_THEME,
            get_template_directory_uri().'/dist/css/app.css',
            ['fonts'],
            apply_filters('release_version', D_VERSION)
        );

        /**
         * Register Javascript
         */
        wp_deregister_script('jquery');

        wp_register_script(
            'jquery',
            '//code.jquery.com/jquery-3.4.1.min.js',
            [],
            '3.4.1',
            true
        );
        wp_script_add_data('jquery', 'crossorigin', 'anonymous');

        wp_register_script(
            'popper',
            '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js',
            ['jquery'],
            '1.16.0',
            true
        );
        wp_register_script(
            D_THEME,
            get_template_directory_uri().'/dist/js/app.js',
            ['jquery', 'popper'],
            apply_filters('release_version', D_VERSION),
            true
        );

        wp_localize_script(
            D_THEME,
            D_THEME.'localize',
            [
                'ajax_url'     => admin_url('admin-ajax.php'),
                'ajax_nonce'   => wp_create_nonce(D_THEME),
                'translations' => [
                    'read_more' => get_field('read_more', 'options'),
                    'read_less' => get_field('read_less', 'options'),
                ],
            ]
        );

        wp_enqueue_script(D_THEME);
    }

    /**
     * Set a acc of dev background
     */
    public function setBackground()
    {
        if (! defined('WP_ENV')) {
            return;
        }

        $image = '';
        switch (WP_ENV) {
            case 'development':
                $image = 'development';
                break;
            case 'accept':
            case 'staging':
//                $image = 'accept';
                break;
            case 'production':
            default:
                $image = '';
                break;
        }
        if (empty($image)) {
            return;
        }

        $image = get_template_directory_uri().'/dist/img/bg_'.$image.'.png';
        echo "<style>body{ background-image: url('$image');}</style>";
    }
}
