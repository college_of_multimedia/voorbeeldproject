<?php

namespace Voorbeeld\Front;

class Filters
{
    /**
     * Filters constructor.
     */
    public function __construct()
    {
        //add_filter('release_version', [$this, 'releaseVersion'], 10, 1);
    }

    /**
     * Include the release number in the version number
     *
     * @param int $version
     *
     * @return float|int|string
     */
    public function releaseVersion($version = 0)
    {
        if (empty($version)) {
            $version = D_VERSION;
        }

        preg_match('/releases\/([0-9]*)\/web/', __dir__, $release);

        if (empty($release) || ! isset($release[1])) {
            return $version;
        }

        return $version.'.'.$release[1];
    }
}
