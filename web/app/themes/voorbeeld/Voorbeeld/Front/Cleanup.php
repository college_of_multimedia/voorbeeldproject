<?php

namespace Voorbeeld\Front;

class Cleanup
{
    public function __construct()
    {
        /*
         * launching operation cleanup
         */
        add_action('init', [$this, 'cleanupHead']);
        add_action('init', [$this, 'killAllEmoji']);
        add_action('wp_enqueue_scripts', [$this, 'cleanupScripts']);
        add_filter('the_generator', [$this, 'removeRssVersion']);

        add_filter('login_headerurl', [$this, 'cleanupAdminLoginHeaderurl']);
        add_filter('login_headertext', [$this, 'cleanupAdminLoginHeadertitle']);
        add_action('login_head', [$this, 'myLoginLogo']);

        add_filter('script_loader_tag', [$this, 'cleanScriptLoaderTag']);
        add_filter('get_bloginfo_rss', [$this, 'removeDefaultDescription']);

        add_action('after_setup_theme', [$this, 'removeAdminBar']);
    }

    /**
     * Remove the utter madness that is emoji
     */
    public static function killAllEmoji()
    {
        add_filter('emoji_svg_url', '__return_false');

        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
    }

    /**
     * Clean up output of <script> tags
     *
     * @param $input
     *
     * @return mixed|string
     */
    public function cleanScriptLoaderTag($input)
    {
        if (! $input) {
            return '';
        }
        $input = str_replace("type='text/javascript' ", '', $input);

        return $input;
    }

    /**
     * Don't return the default description in the RSS feed if it hasn't been changed.
     *
     * @param $bloginfo
     *
     * @return string
     */
    public function removeDefaultDescription($bloginfo)
    {
        $default_tagline = 'Just another WordPress site';

        return ($bloginfo === $default_tagline) ? '' : $bloginfo;
    }


    /*
     * Remove the `wp-block-library.css` file from `wp_head()`
     *
     * @uses   wp_dequeue_style
     */

    public function cleanupScripts()
    {
        wp_dequeue_style('wp-block-library');
    }

    /**
     * Clean up head
     */
    public function cleanupHead()
    {
        // Remove WP 4.9+ dns-prefetch nonsense
        remove_action('wp_head', 'wp_resource_hints', 2);

        // EditURI link
        remove_action('wp_head', 'rsd_link');

        // Category feed links
        remove_action('wp_head', 'feed_links_extra', 3);

        // Post and comment feed links
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'rest_output_link_wp_head', 10);
        remove_action('wp_head', 'print_emoji_detection_script', 7);

        // Windows Live Writer
        remove_action('wp_head', 'wlwmanifest_link');

        // Index link
        remove_action('wp_head', 'index_rel_link');

        // Previous link
        remove_action('wp_head', 'parent_post_rel_link', 10);

        // Start link
        remove_action('wp_head', 'start_post_rel_link', 10);

        // Canonical
        remove_action('wp_head', 'rel_canonical', 10);

        // Shortlink
        remove_action('wp_head', 'wp_shortlink_wp_head', 10);

        // Links for adjacent posts
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

        // Remove author tag in DOM
        remove_action('wp_head', 'wp_generator');

        // Disable XML-RPC
        add_filter('xmlrpc_enabled', '__return_false');

        remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
        remove_action('wp_head', 'wp_oembed_add_host_js');
        remove_action('rest_api_init', 'wp_oembed_register_route');
        remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

        // Remove WP version from css
        add_filter('style_loader_src', [$this, 'removeWpVersionCssJs'], 9999);

        // Remove WP version from scripts
        add_filter('script_loader_src', [$this, 'removeWpVersionCssJs'], 9999);

        add_filter('gform_init_scripts_footer', '__return_true');
    }

    /**
     * remove WP version from RSS
     *
     * @return string
     */
    public function removeRssVersion()
    {
        return '';
    }

    /**
     * remove WP version from scripts
     *
     * @param $src
     *
     * @return string
     */
    public function removeWpVersionCssJs($src)
    {
        if (strpos($src, 'ver=')) {
            $src = remove_query_arg('ver', $src);
            $src .= '?ver='.apply_filters('release_version', D_VERSION);
        }

        return $src;
    }


    /**
     * Remove the admin bar for users
     */
    public function removeAdminBar()
    {
        if (!current_user_can('administrator') && !is_admin()) {
            show_admin_bar(false);
        }
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function cleanupAdminLoginHeaderurl($url)
    {
        return site_url('../');
    }

    /**
     * @param $url
     *
     * @return string
     */
    public function cleanupAdminLoginHeadertitle($title)
    {
        return get_option('blogname');
    }

    /**
     * Add a custom logo to the wp login page
     */
    public function myLoginLogo()
    {
        echo '<style type="text/css">
                body.login div#login h1 a {
                    background-image: url( \''.get_template_directory_uri().'/dist/img/login_logo.png\');
                    margin-top: 30px;
                    padding-bottom: 0px;
                    background-size: auto;
                    width: 255px;
                    height:36px;
                    font-family: "Arial Hebrew", Arial, sans-serif;
                }
                body #login {
                    width: 352px !important;
                }
                body.login-action-login #loginform {
                    min-width: 310px !important;
                    background: rgba(255,255,255,0.4);
                    border: solid 1px rgba(0,0,0,1);
                }
                body.login-action-login #loginform .button-large {
                   background-color: #7dad8e;
                   border-color: #dbbebd;
                   text-shadow: none;
                   box-shadow: none;
                   color: #1c2123;
                }
	        </style>';
    }
}
