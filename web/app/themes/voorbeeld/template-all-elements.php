<?php
/*
 * Template Name: All Elements
 */

get_header(); ?>

<div class="js_slickCarouselHeading slick-carousel-heading">
    <img src="https://placeimg.com/900/300/arch" alt="">
    <img src="https://placeimg.com/800/300/people" alt="">
    <img src="https://placeimg.com/1200/800/nature" alt="">
    <img src="https://placeimg.com/800/900/animals" alt="">
    <img src="https://placeimg.com/800/900/animals" alt="">
</div>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-5 mb-4">
            <a href="#">
                <div class="product-card">
                    <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                </div>
            </a>
        </div>
        <div class="col-md-5 mb-4">
            <a href="#">
                <div class="product-card">
                    <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                </div>
            </a>
        </div>
        <div class="col-md-5 mb-4">
            <a href="#">
                <div class="product-card">
                    <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                </div>
            </a>
        </div>
        <div class="col-md-5 mb-4">
            <a href="#">
                <div class="product-card">
                    <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                </div>
            </a>
        </div>
    </div>
</div>

<!--BRAND CAROUSEL-->
<div class="js_slickCarouselBrands slick-carousel-brands w-100">
    <a href="" class="brand-card"><img src="https://placeimg.com/150/150/arch" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/200/100/people" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/180/80/nature" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/260/100/animals" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/180/80/nature" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/160/60/animals" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/150/50/arch" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/100/100/people" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/180/80/nature" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/160/60/animals" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/180/80/nature" alt="" class="brand-card__img"></a>
    <a href="" class="brand-card"><img src="https://placeimg.com/160/60/animals" alt="" class="brand-card__img"></a>
</div>


<!--BRAND MAIN PAGE-->
<div class="container">
    <div class="row">
        <div class="col-12 py-3 bg-primary d-lg-flex">
            <strong>Aida Imaging</strong>
            <span
                class="mx-2">Ferri animal offendit te mel, duo cu quas semper lucilius! Commodo salutandi his no.</span><a
                href="" class="fas fa-external-link-alt ml-auto text-body my-auto"></a>
        </div>
    </div>
    <div class="row my-4">
        <div class="col-md-3 text-center">
            <img src="https://placeimg.com/500/200/nature" alt="" class="img-fluid">
        </div>
        <div class="col-md-9">
            <p class="display-4">Vis vero tritani appetere ne, eum stet commune iudicabit ea. Eos no viderer fierent
                pertinacia, ei vidisse intellegebat deterruisset vix, eum case utinam vulputate no. Duo veritus docendi
                facilisi at, sea deserunt reprehendunt at, sed dico apeirian urbanitas ad. Duo eligendi efficiendi
                interpretaris ad. Cum ad quando eloquentiam signiferumque, ea his tantas primis laoreet. Ea nusquam
                consequat, vis commune lucilius et!</p>
        </div>
    </div>
    <div class="row justify-content-center mt-4">
        <?php for ($i = 0; $i < 6; $i++) : ?>
            <div class="col-md-6 col-lg-4 mb-4">
                <a href="#" class="text-body">
                    <div class="bg-primary px-3 py-2"><h5 class="m-0">Duo eligendi efficiendi</h5></div>
                    <div class="product-card">
                        <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                    </div>
                </a>
                <p>Cu eos altera antiopam, honestatis scripse
                    rit sit at, putent utroque fuisset cu eam.
                    Decore civibus eum in. Ut mei quaeque quo
                    molestie rationibus, ex his suscipit
                    interpretaris.</p>
            </div>
        <?php endfor; ?>
    </div>
    <div class="row mt-4 justify-content-center">
        <div class="col-lg-8 text-center">
            <video width="100%" controls>
                <source src="mov_bbb.mp4" type="video/mp4">
                <source src="mov_bbb.ogg" type="video/ogg">
                Your browser does not support HTML5 video.
            </video>
            <p>Ferri animal offendit te mel, duo cu quas semper lucilius! Commodo salutandi his no.</p>
        </div>
    </div>
</div>

<!--BRAND SUB SUB CATEGORIES-->
<div class="container product-list">
    <div class="row justify-content-center mt-4">
        <?php for ($i = 0; $i < 8; $i++) : ?>
            <div class="col-md-6 col-lg-4 col-xl-3 mb-4 text-center">
                <a href="#" class="text-body">
                    <div class="product-card">
                        <img src="https://placeimg.com/640/480/any" alt="" class="product-card__img">
                    </div>
                    <div class="bg-primary px-3 py-2"><h5 class="m-0 font-size-base">Duo eligendi efficiendi</h5></div>
                </a>
                <small>Cu eos altera antiopam, honestatis scripse
                    rit sit at, putent utroque fuisset cu eam interpretaris.</small>
                <small class="d-block font-weight-bolder mt-1">MSRP USD 123.77</small>
            </div>
        <?php endfor; ?>
    </div>
</div>

<!--SHOP PAGE-->
<div class="container">
    <div class="row">
        <div class="col-12 py-3 bg-primary d-lg-flex">
            <strong>Aida Imaging</strong>
            <span
                class="mx-2">Ferri animal offendit te mel, duo cu quas semper lucilius! Commodo salutandi his no.</span>
            <span class="ml-auto">P4567RT444</span><a href="" class="fas fa-external-link-alt ml-3 text-body my-auto"></a>
        </div>
    </div>

    <div class="row my-4 justify-content-end">
        <div class="col-md-6 col-xl-4 mx-xl-auto text-center">
            <div class="js_slickCarouselProduct product-list">
                <div class="product-card">
                    <img src="https://placeimg.com/900/300/arch" alt="" class="product-card__img">
                </div>
                <div class="product-card">
                    <img src="https://placeimg.com/800/300/people" alt="" class="product-card__img">
                </div>
                <div class="product-card">
                    <img src="https://placeimg.com/1200/800/nature" alt="" class="product-card__img">
                </div>
                <div class="product-card">
                    <img src="https://placeimg.com/800/900/animals" alt="" class="product-card__img">
                </div>
            </div>
        </div>
        <div class="col-md-6 d-flex flex-column">
            <img src="https://placeimg.com/100/50/nature" alt="" class="ml-auto img-fluid mb-3">
            <div class="mx-auto">
                <p class="font-size-medium m-0">Ferri animal offendit mel 2244X-55</p>
                <p class="font-size-small m-0">Sit omnes tritani erroribus at</p>
                <p class="font-size-small my-3">product code: ad007</p>
                <div><span class="font-size-xlarge">MSRP € 997.00</span> <small>(ex. VAT)</small></div>
            </div>

            <div class="mt-auto d-flex justify-content-between align-items-center">
                <div class=""><a href="" class="ml-2 text-body"><?= __('info request', D_DOMAIN) ?><i class="ml-3 icon-email icon-2x"></i></a></div>
                <div class="d-flex align-items-center">
                    <a href="" class="ml-2 text-body"><?= __('quantity', D_DOMAIN) ?></a>
                    <div style="width: 30px" class="ml-3">
                        <div class="input-group-prepend">
                            <button class="btn btn-light btn-sm w-100 py-0" id="plus-btn"><i class="fa fa-caret-up"></i></button>
                        </div>
                        <input id="qty_input" class="w-100 text-center" value="1" min="1">
                        <div class="input-group-prepend">
                            <button class="btn btn-light btn-sm w-100 py-0" id="minus-btn"><i class="fa fa-caret-down"></i></button>
                        </div>
                    </div>
                </div>
                <div class=""><a href="" class="ml-2 text-body"><?= __('add to your wish list', D_DOMAIN) ?><i class="ml-3 fas fa-shopping-bag"></i></a></div>
            </div>
        </div>
    </div>

<!--    todo: Tabs -->

    <div class="row mt-4 justify-content-center">
        <div class="col-lg-8 text-center">
            <video width="100%" controls>
                <source src="mov_bbb.mp4" type="video/mp4">
                <source src="mov_bbb.ogg" type="video/ogg">
                Your browser does not support HTML5 video.
            </video>
            <p>Ferri animal offendit te mel, duo cu quas semper lucilius! Commodo salutandi his no.</p>
        </div>
    </div>
</div>

<!--FOOTER-->
<footer class="bg-primary py-5">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-12">
                        <h3 class="mb-3">sitemap</h3>
                    </div>
                    <div class="col-lg-3">
                        <h4>brands</h4>
                    </div>
                    <div class="col-lg-3">
                        <h4>brands</h4>
                    </div>
                    <div class="col-lg-3">
                        <h4>brands</h4>
                    </div>
                    <div class="col-lg-3">
                        <h4>brands</h4>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 mt-4 mt-xl-0">
                <div class="row">
                    <div class="col-12 mb-3">
                        <h3>newsletter</h3>
                    </div>
                    <div class="col-12 border-left-xl">
                        <p>Keep up-to-date! Our newsletter brings you latest news about products, releases, events and
                            other stuff that might be of interest to you.</p>
                        <i class="icon-email icon-2x"></i>
                        <a href="" class="ml-3 text-body">subscribe</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 ml-auto mt-4">
                <a href="" class="text-body text-decoration-none">
                    <i class="icon-facebook icon-2x"></i> </a>
                <a href="" class="text-body text-decoration-none">
                    <i class="icon-instagram icon-2x"></i> </a>
                <a href="" class="text-body text-decoration-none">
                    <i class="icon-youtube icon-2x"></i> </a>
                <a href="" class="text-body text-decoration-none">
                    <i class="icon-twitter icon-2x"></i> </a>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <small>&copy; <?= date('Y') ?> Voorbeeld ı privacy statement ı cookie statement</small>
            </div>
        </div>
    </div>
</footer>

<hr class="mt-5 mb-5" />
<?php get_footer() ?>
