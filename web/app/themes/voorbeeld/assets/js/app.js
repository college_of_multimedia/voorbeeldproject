/*
 global
 jQuery, define
 */

require('bootstrap');

(function (factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (typeof exports !== 'undefined') {
    module.exports = factory(require('jquery'));
  } else {
    factory(jQuery);
  }

}(function ($) {
  'use strict';

  function getBootstrapDeviceSize() {
    return $('#users-device-size').find('div:visible').first().attr('id');
  }
  getBootstrapDeviceSize();
}));
