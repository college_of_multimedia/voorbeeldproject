Code van:
https://bitbucket.org/college_of_multimedia/voorbeeldproject/src/master/

# Step 1: Getting started

## 2.1: Composer
* `composer update`
* Edit .env file (for you local dev setup)

## 2.2: Valet
```
valet link
valet secure
```

## 2.3: Deployer
See deployer for additional information / explanation.

# PSR/2 Validating
We are using: https://github.com/squizlabs/PHP_CodeSniffer
```
Available options:
 ./vendor/bin/phpcs -h
 ./vendor/bin/phpcbf -h
```

You can run the psr/2 test with this command:
```
./vendor/bin/phpcs --standard=PSR2 --ignore=MenuWalker.php --colors -p --extensions=php -n ./web/app/themes/voorbeeld/
```

You can run the js lint with this command:
```shell script
npm run lint
```