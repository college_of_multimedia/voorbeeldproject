<?php
get_header();

if (isset($_GET['debug']) && WP_DEBUG) {
    dump([
        'is_home'              => is_home(),
        'is_front_page'        => is_front_page(),
        'is_category'          => is_category(),
        'get_query_var( cat )' => get_query_var('cat'),
        'is_single'            => is_single(),
        'is_page'              => is_page(),
        'is_singular'          => is_singular(),
        'is_woocommerce'       => is_woocommerce(),
        'is_product_category'  => is_product_category(),
        'is_product_tag'       => is_product_tag(),
        'get_post_type'        => get_post_type(get_the_ID()),
        'is_404'               => is_404(),
        'is_search'            => is_search(),
    ]);
}

switch (true) {
    case is_front_page():
        get_template_part('views/page/home');
        break;
    case is_home():
        get_template_part('views/blog/list');
        break;
    case is_404():
        get_template_part('views/page/404');
        break;
    case is_search():
        get_template_part('views/search/index');
        break;
    case (is_woocommerce() && is_product_category()):
        get_template_part('views/shop/category_index');
        break;
    case (! is_single() && 'reseller' === get_post_type(get_the_ID())):
        get_template_part('views/reseller/list');
        break;
    case (is_single() && 'reseller' === get_post_type(get_the_ID())):
        get_template_part('views/reseller/show');
        break;
    default:
        get_template_part('views/page/show');
        break;
}

get_footer();
