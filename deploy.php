<?php

namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'Voorbeeld');
set('timezone', 'Europe/Amsterdam');
set('repository', 'git@bitbucket.org:college_of_multimedia/voorbeeldproject.git');
set('keep_releases', 4);
set('wpcli_command', 'wp');
set('allow_anonymous_stats', false);
set('theme_dir', 'web/app/themes/voorbeeld');

// Shared files/dirs between deploys
set('shared_dirs', [
    'logs',
    'provision',
    'web/app/uploads',
]);
set('shared_files', [
    '.env',
    'web/.htaccess',
]);

// Writable dirs by web server
set('writable_dirs', [
    'logs',
    'web/app/uploads',
]);

// Flush permalinks
desc( 'Flush permalinks' );
task('dragonet:flush_permalinks', function () {
    $usr = '';
    if ('acc' === input()->getArgument('stage')) {
        $usr = 'acc:jasper@';
    }
    write('curl {{protocol}}://' . $usr . '{{domain}}?c=T2Nn2Y8AsvnEdRfA4r'."\n");
    run('curl {{protocol}}://' . $usr . '{{domain}}?c=T2Nn2Y8AsvnEdRfA4r');
    write('{{protocol}}://' . $usr . '{{domain}}/wp/wp-admin/options-permalink.php'."\n");
});

// Copy the assets from local
desc( 'Copy Assets' );
task('dragonet:copy_assets', function () {
    write('Compile assets'."\n");
    runLocally('cd ./{{theme_dir}}; npm run dev');
    upload('{{theme_dir}}/assets', '{{release_path}}/{{theme_dir}}');
    upload('{{theme_dir}}/dist', '{{release_path}}/{{theme_dir}}');
});

// Remove dev folder
desc( 'Remove excluded folders' );
task('dragonet:remove_excluded', function () {
    run('rm -rf {{release_path}}/_dev');
});


// Hosts
host('accept')
    ->hostname('cmm.nl')
    ->user('voorbeelduser')
    ->multiplexing(true)
    ->set('deploy_path', '/home/deb124505/domains/voorbeeld.cmm.nl')
    ->set('http_user', 'voorbeelduser')
    ->set('composer_options',
        'install --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction --ignore-platform-reqs')
    ->set('writable_mode', 'chmod')
    ->set('writable_chmod_mode', '0775')
    ->set('writable_use_sudo', false)
    ->set('domain', 'voorbeeld.cmm.nl')
    ->set('db_name', 'deb124505_voorbeeld')
    ->set('db_user', 'deb124505_vb')
    ->set('db_pass', 'xxxx')
    ->set('db_host', 'localhost')
    ->set('db_prefix', 'vb_')
    ->set('protocol', 'https')
    ->set('domain-prepend-www', false)// prepend www to the domain
    ->set('wp_env', 'staging')
    ->set('git_branch', '_accept')
    ->stage('acc');
	
host('prod')
    ->hostname('voorbeeld.nl')
    ->user('')
    ->multiplexing(true)
    ->set('deploy_path', '/domains/voorbeeld.nl')
    ->set('http_user', '')
    ->set('composer_options',
        'install --no-dev --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction --ignore-platform-reqs')
    ->set('writable_mode', 'chmod')
    ->set('writable_chmod_mode', '0775')
    ->set('writable_use_sudo', false)
    ->set('domain', 'voorbeeld.com')
    ->set('db_name', '')
    ->set('db_user', '')
    ->set('db_pass', '')
    ->set('db_host', 'localhost')
    ->set('db_prefix', 'vb_')
    ->set('protocol', 'https')
    ->set('domain-prepend-www', false)// prepend www to the domain
    ->set('wp_env', 'staging')
    ->set('git_branch', 'master')
    ->stage('prod');

// Tasks
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'dragonet:copy_assets',
    'dragonet:remove_excluded',
    'deploy:symlink',
    'deploy:unlock',
    'dragonet:flush_permalinks',
    'cleanup',
    'success',
]);
task('flush', [
    'dragonet:flush_permalinks',
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');


